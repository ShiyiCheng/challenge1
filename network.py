import sys

map = {}


def build(pair):
    start = pair[0]
    end = pair[1]
    next_node = map.get(start, [])
    next_node.append(end)
    map[start] = next_node


def identify_router():
    if len(map) == 0:
        return {}
    count = {}
    for key, value in map.items():
        start = key
        for end in value:
            startval = count.get(start, 0) + 1
            endval = count.get(end, 0) + 1
            count[start] = startval
            count[end] = endval

    largestnum = 0
    res = []

    print("count")
    print(count)

    for key, value in count.items():
        if value > largestnum:
            res.clear()
            res.append(key)
            largestnum = value
        elif value == largestnum:
            res.append(key)
    return res


build([2, 4])
build([4, 6])
build([6, 2])
build([2, 5])
build([5, 6])

print(map)
print(identify_router())

# Time complexity of build: o(1);
# Time Complexity of identify_router: o(n)
