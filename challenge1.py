def compress(s):
    n = len(s)
    cur_count = 0  # cur_count represents the number of the current letter. For example, aaa=>a3, cur_count = 3
    cur_letter = ''
    res = ''
    for i in range(n):
        if i == 0 or s[i] != s[i - 1]:
            res = res + cur_letter
            if cur_count > 1:
                res = res + str(cur_count)

            cur_letter = s[i]
            cur_count = 1
        else:
            cur_count = cur_count + 1

    res = res + cur_letter
    if cur_count > 1:
        res = res + str(cur_count)

    return res

# Basic idea: Iterate every element of a string, and use cur_letter and cur_count to record the current letter and
# current number of the letter. If the current letter is not the same as the previous letter, we add the current
# letter and count to the result.

# Time Complexity: o(n) Just using one iteration
# Space Complexity: o(1) Use two variable to record the current letter and the number of the current letter. The result
# is saved by a string named 'res'.

